# modulos-ds

modulos ds

# Prerequisites
- Python 3.8
- Make (used as a task runner)

# Development setup
To init the development environment, you should just run

```shell
make sync
```

This will:
- Initialize a git repository
- Create python virtual environment on `.venv` with default dev dependencies installed
- Install pre-commit hooks
- Instal and create both requirements.txt and requirements-dev.txt

## Installing new dependencies
You can add the package name on requirements.in or requirements-dev.in, depending if it's a development or a runtime dependency.

```ini
django 				# If you don't care about an specific version
pyflakes==2.1.1		# If you need an specific version
```

After that, run the command to update and install the new dependencies inside the virtual environment.

```shell
make sync
```

## Running scripts inside virtual env
```shell
source .venv/bin/activate.fish
python modulos_ds/modulos_ds.py
```

Or just run

```shell
make run
```

If you are running the main python file on `modulos_ds/modulos_ds.py`

# Running tests
`pytest` is used for running tests inside the project.

You can use the Makefile script to run all the test battery
```shell
make test
```

# Linting
To lint the code, you can use
```shell
make lint
```
