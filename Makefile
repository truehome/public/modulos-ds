VENV_NAME?=.venv
VENV_ACTIVATE=. $(VENV_NAME)/bin/activate
PYTHON=$(if $(CI), /usr/bin/env python3 ,$(VENV_NAME)/bin/python3)

all: init

clean: clean-venv

clean-venv:
	@rm -rf .venv

init: venv

venv: .git $(VENV_ACTIVATE)

$(VENV_ACTIVATE):
	test -d $(VENV_NAME) || python3 -m venv $(VENV_NAME)
	$(PYTHON) -m pip install -qU pip pip-tools pre-commit
	$(PYTHON) -m pre_commit install
.git:
	git init

test: $(if $(CI), ,init)
	$(PYTHON) -m pytest -vv tests

lint: $(if $(CI), ,init)
	$(PYTHON) -m flake8 --statistics --count

run: $(if $(CI), ,init)
	$(PYTHON) modulos_ds/modulos_ds.py

requirements-dev.txt: requirements-dev.in
	$(PYTHON) -m piptools compile requirements-dev.in

requirements.txt: requirements.in
	$(PYTHON) -m piptools compile requirements.in

sync: init requirements.txt requirements-dev.txt
	$(PYTHON) -m piptools sync requirements.txt requirements-dev.txt


.PHONY: clean clean-venv
